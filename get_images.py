#!/usr/bin/env python
# -*- coding: utf-8 -
import click
import requests
from os import path


class FileExistsAlreadyError(IOError):
    pass


def download_file(file_url, output_file, force_overwrite=False):
    if path.exists(output_file) and not force_overwrite:
        raise FileExistsAlreadyError(
            "file exists already for {}\n{}".format(file_url, output_file))
    response = requests.get(file_url)
    response.raise_for_status()
    with open(output_file, 'wb') as output_fd:
        for chunk in response.iter_content(chunk_size=2**12):
            output_fd.write(chunk)


@click.command()
@click.option('--input-file', '-i', default='-', type=click.File('r'))
@click.option('--output-dir', '-o', default='.',
              type=click.Path(exists=True, file_okay=False, writable=True))
@click.option('--force-overwrite', is_flag=True)
def main(input_file, output_dir, force_overwrite):
    """
    This script reads a list of URLs either from STDIN or from the provided
    input file. It will download the specified files and save them in the
    output directory.

    \b
    It expects one URL per line. Lines starting with '#' and empty lines are
    ignored. Successfully downloaded URLs will be printed to STDOUT while
    failed ones will be printed to STDERR (with the error in a comment above).

    \b
    Options:
    --input-file, -i    The input file to read URLs from. Default: STDIN
    --output-dir, -o    The output directory to save all the downloaded images
                        to. Default: current directory
    --force-overwrite   Normally existing files won't be overwritten and the
                        download will be reported as failed. This option forces
                        the download to occur and overwrite the existing file.

    \b
    Examples:
         cat urls.txt | get_images.py -o download_target/
         get_images.py -i urls.txt -o download_target/ > successfully_downloaded_urls.txt
         get_images.py -i urls.txt -o download_target/ 2> failed_urls.txt
    """
    for file_url in input_file:
        file_url = file_url.strip()
        if len(file_url) == 0 or file_url.startswith('#'):
            continue
        file_name = path.basename(file_url)
        output_file = path.join(output_dir, file_name)
        try:
            download_file(file_url, output_file, force_overwrite)
        except FileExistsAlreadyError:
            click.echo("# output file exists already: {}\n{}".format(
                output_file, file_url), err=True)
        except Exception as exc:
            click.echo("# {}\n{}".format(exc, file_url), err=True)
        else:
            click.echo(file_url)


if __name__ == '__main__':
    main()
