
# Installation

Create a virtual environment and install the requirements.

```shell
virtualenv venv/
source ./venv/bin/activate
pip install -r requirements.txt
```

It works with both Python 2 and 3.

# Usage

You can call this script in various ways.

```shell
cat urls.txt | ./get_images.py -o download_target/
./get_images.py -i urls.txt -o download_target/ > successfully_downloaded_urls.txt
./get_images.py -i urls.txt -o download_target/ 2> failed_urls.txt
```

`./get_images.py --help` will provide you with a list of options and usage examples.

# File Format

It expects one URL per line.
Whitespace at the beginning and end of each line is removed.
Lines starting with "#" and empty lines are ignored.
Successfully downloaded URLs will be printed to `STDOUT` while failed ones will be printed to `STDERR` (with the error in a comment above).

## Example

A sample input file like this:

```txt
#
# Download all the images!!!
#

http://example.tld/file1.jpg
http://example.tld/file2.jpg
http://example.tld/file3.jpg
http://example.tld/file4.jpg

# http://example.tld/file2.jpg

# duplicate
http://example.tld/file3.jpg
```

will produce an output like the following:

STDOUT:

    http://example.tld/file1.jpg
    http://example.tld/file2.jpg
    http://example.tld/file3.jpg
    http://example.tld/file4.jpg

STDERR:

    # output file exists already: download_target/file3.jpg
    http://example.tld/file3.jpg


# Running The Tests

Assuming you're still in your virtualenv you created during installation you can run the tests as follows:

```shell
pip install -Ur test-requirements.txt
py.test tests/
```



# Given Task

    Given a plaintext file containing URLs, one per line, e.g.:
    http://mywebserver.com/images/271947.jpg
    http://mywebserver.com/images/24174.jpg
    http://somewebsrv.com/img/992147.jpg

    Write a script that takes this plaintext file as an argument and downloads all images, storing them on the local hard disk.
    Approach the problem as you would any task in a normal day’s work: as if this code will be used in important live systems, modified later on by other developers, and so on.
    Please use python for your solution. We prefer to receive your code in GitHub or a similar repository.
