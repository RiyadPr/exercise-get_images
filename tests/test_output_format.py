# -*- coding: utf-8 -
import pytest
import subprocess


@pytest.fixture
def input_file_with_single_url(input_file, image_url):
    with open(input_file, 'w+') as f:
        f.write('{}\n'.format(image_url))
    return input_file


@pytest.fixture
def input_file_with_bad_url(input_file, bad_url):
    with open(input_file, 'w+') as f:
        f.write('{}\n'.format(bad_url))
    return input_file


def test_prints_downloaded_url_to_stdout(script, input_file_with_single_url,
                                         output_dir, output_stream, image_url):
    subprocess.call(
        [script, '-i', input_file_with_single_url, '-o', output_dir],
        stdout=output_stream)
    output_stream.seek(0)
    output = output_stream.read().decode()
    assert image_url in output


def test_doesnt_print_bad_url_to_stdout(script, input_file_with_bad_url,
                                        output_dir, output_stream):
    subprocess.call([script, '-i', input_file_with_bad_url, '-o', output_dir],
                    stdout=output_stream)
    output_stream.seek(0)
    output = output_stream.read().decode()
    assert output == ''


def test_prints_bad_url_to_stderr(script, input_file_with_bad_url, output_dir,
                                  output_stream, bad_url):
    subprocess.call([script, '-i', input_file_with_bad_url, '-o', output_dir],
                    stderr=output_stream)
    output_stream.seek(0)
    output = output_stream.read().decode()
    assert bad_url in output


def test_prints_comment_for_bad_url_to_stderr(script, input_file_with_bad_url,
                                              output_dir, output_stream):
    subprocess.call([script, '-i', input_file_with_bad_url, '-o', output_dir],
                    stderr=output_stream)
    output_stream.seek(0)
    output = output_stream.read().decode()
    assert output.startswith("# HTTPSConnectionPool(host='fake.tld', port=443)"
                             ": Max retries exceeded with url: /bad_file (Caus"
                             "ed by NewConnectionError('<requests.packages.url"
                             "lib3.connection.VerifiedHTTPSConnection")
