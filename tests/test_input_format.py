# -*- coding: utf-8 -
import subprocess


def test_accepts_urls(script, input_file, output_dir, image_url):
    with open(input_file, 'w+') as f:
        f.write('{}\n{}\n{}\n'.format(image_url, image_url, image_url))
    stdout = subprocess.check_output(
        [script, '-i', input_file, '-o', output_dir])
    assert image_url in stdout.decode()


def test_skips_empty_lines(script, input_file, output_dir):
    with open(input_file, 'w+') as f:
        f.write('\n\n\n')
    stdout = subprocess.check_output(
        [script, '-i', input_file, '-o', output_dir])
    assert stdout.decode() == ''


def test_skips_lines_starting_with_sharp(script, input_file, output_dir):
    with open(input_file, 'w+') as f:
        f.write('\n# bla\n')
    stdout = subprocess.check_output(
        [script, '-i', input_file, '-o', output_dir])
    assert stdout.decode() == ''
