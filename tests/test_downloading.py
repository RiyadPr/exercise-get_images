# -*- coding: utf-8 -
import os
import pytest
import subprocess


@pytest.fixture
def input_file_with_single_url(input_file, image_url):
    with open(input_file, 'w+') as f:
        f.write('{}\n'.format(image_url))
    return input_file


@pytest.fixture
def input_file_with_same_url_multiple_times(input_file, image_url):
    with open(input_file, 'w+') as f:
        f.write('{}\n{}\n'.format(image_url, image_url))
    return input_file


@pytest.fixture
def input_file_with_bad_url(input_file, bad_url):
    with open(input_file, 'w+') as f:
        f.write('{}\n'.format(bad_url))
    return input_file


def test_creates_file_for_good_url(script, input_file_with_single_url,
                                   output_dir, image_url):
    subprocess.call(
        [script, '-i', input_file_with_single_url, '-o', output_dir])
    file_name = os.path.basename(image_url)
    assert len(os.listdir(output_dir)) == 1
    assert file_name in os.listdir(output_dir)


def test_fail_if_file_is_to_be_overwritten(script,
                                           input_file_with_same_url_multiple_times,
                                           output_dir, output_stream):
    subprocess.call(
        [script, '-i', input_file_with_same_url_multiple_times,
         '-o', output_dir],
        stderr=output_stream)
    output_stream.seek(0)
    output = output_stream.read().decode()
    assert 'output file exists already' in output


def test_force_overwriting_existing_file(script,
                                         input_file_with_same_url_multiple_times,
                                         output_dir, output_stream, image_url):
    subprocess.call(
        [script, '-i', input_file_with_same_url_multiple_times,
         '-o', output_dir, '--force-overwrite'],
        stderr=output_stream)
    output_stream.seek(0)
    output = output_stream.read().decode()
    assert output == ''
    file_name = os.path.basename(image_url)
    assert len(os.listdir(output_dir)) == 1
    assert file_name in os.listdir(output_dir)


def test_bad_url_doesnt_create_file(script, input_file_with_bad_url,
                                    output_dir):
    subprocess.call(
        [script, '-i', input_file_with_bad_url, '-o', output_dir])
    assert len(os.listdir(output_dir)) == 0
