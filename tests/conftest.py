# -*- coding: utf-8 -
import pytest
import os
import shutil
from tempfile import gettempdir, mkdtemp, mkstemp, TemporaryFile


@pytest.fixture
def bad_url():
    return 'https://fake.tld/bad_file'


@pytest.fixture
def image_url():
    return "https://en.wikipedia.org/static/images/project-logos/enwiki.png"


@pytest.fixture
def input_file(request):
    fd, file_path = mkstemp()
    def _remove_temp_file():
        os.unlink(file_path)
    request.addfinalizer(_remove_temp_file)
    return file_path


@pytest.fixture
def input_stream():
    return TemporaryFile()


@pytest.fixture
def non_existent_file():
    return os.path.join(gettempdir(), 'foo_bar.txt')


@pytest.fixture
def output_dir(request):
    dir_path = mkdtemp()
    def _remove_temp_dir():
        shutil.rmtree(dir_path)
    request.addfinalizer(_remove_temp_dir)
    return dir_path


@pytest.fixture
def output_stream():
    return TemporaryFile()


@pytest.fixture
def script():
    return os.path.join(os.path.dirname(__file__), '../get_images.py')


@pytest.fixture
def temp_file(request):
    fd, file_path = mkstemp()
    def _remove_temp_file():
        os.unlink(file_path)
    request.addfinalizer(_remove_temp_file)
    return file_path


@pytest.fixture
def temp_dir(request):
    dir_path = mkdtemp()
    def _remove_temp_dir():
        shutil.rmtree(dir_path)
    request.addfinalizer(_remove_temp_dir)
    return dir_path
