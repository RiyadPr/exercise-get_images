# -*- coding: utf-8 -
import subprocess


def test_accepts_long_input_file_param(script, input_file):
    assert subprocess.call(
        [script, '--input-file={}'.format(input_file)]) == 0


def test_accepts_short_input_file_param(script, input_file):
    assert subprocess.call([script, '-i', input_file]) == 0


def test_rejects_non_existent_input_file(script, non_existent_file):
    assert subprocess.call([script, '-i', non_existent_file]) > 0


def test_rejects_directory_as_input_file(script, temp_dir):
    assert subprocess.call([script, '-i', temp_dir]) > 0


def test_accepts_input_through_stdin(script, input_stream):
    assert subprocess.call([script, '-i', '-'], stdin=input_stream) == 0
