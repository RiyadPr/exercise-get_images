# -*- coding: utf-8 -
import subprocess


def test_accepts_long_output_dir_param(script, output_dir):
    assert subprocess.call(
        [script, '--output-dir={}'.format(output_dir)]) == 0


def test_accepts_short_output_dir_param(script, output_dir):
    assert subprocess.call([script, '-o', output_dir]) == 0


def test_rejects_non_existent_output_dir(script, non_existent_file):
    assert subprocess.call([script, '-o', non_existent_file]) > 0


def test_rejects_file_as_output_dir(script, temp_file):
    assert subprocess.call([script, '-o', temp_file]) > 0
